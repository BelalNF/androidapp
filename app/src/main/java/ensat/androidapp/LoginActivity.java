package ensat.androidapp;


import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEditText;
    private EditText passwordEditText;
    private TextView errorTextView;
    private LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // FacebookSdk.sdkInitialize(getApplicationContext());
        //    AppEventsLogger.activateApp(this);

        if(isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return;
        }

            setContentView(R.layout.activity_login);


        usernameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        errorTextView  = findViewById(R.id.error);
        loginButton = findViewById(R.id.login_button);

        callbackManager = CallbackManager.Factory.create();

        final LoginActivity thos = this;

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Intent intent = new Intent(thos, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onCancel() {
                        errorTextView.setText("Login cancled by user");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        errorTextView.setText("Login or password incorrect");
                    }
                });
    }

    public void login(View v){
        if(usernameEditText.getText().toString().equalsIgnoreCase(passwordEditText.getText().toString())){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }else{
            showLoginFailed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
     }



    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
    private void showLoginFailed() {
        errorTextView.setText("Login or password incorrect");
    }
}
