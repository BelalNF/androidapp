package ensat.androidapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ensat.androidapp.Models.Etablissement;

public class CustomAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Etablissement> listET;
    int sorted = 0;

    public CustomAdapter(LayoutInflater inflater, List<Etablissement> listET) {
        this.listET = listET;
        layoutInflater = inflater;
    }

    @Override
    public int getCount() {
    //used to get how many items in your array
        return listET.size();
    }

    @Override
    public Object getItem(int position) {
        return listET.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = layoutInflater.inflate(R.layout.customlayout,null);
        ImageView imageView = view.findViewById(R.id.imageView2);
        TextView textView1 = view.findViewById(R.id.textViewnom);
        TextView textView2 = view.findViewById(R.id.textViewspec);
        TextView textView3 = view.findViewById(R.id.textViewadresse);

        imageView.setImageBitmap(listET.get(position).getLogo());

        textView1.setText(listET.get(position).getNom());
        textView2.setText(listET.get(position).getFormation()+"/"+listET.get(position).getSpecialite());
        textView3.setText(listET.get(position).getAdresse());


        return view;
    }

    public void sortList(){
        if(sorted==0){
            //fisrst sort ascended
            listET.sort(new Comparator<Etablissement>() {
                @Override
                public int compare(Etablissement o1, Etablissement o2) {
                    int NameCompare = o1.getNom().compareTo(o2.getNom());
                    sorted=1;
                    // 2-level comparison using if-else block
                    return NameCompare;

                }

            });
        }else{
            listET.sort(new Comparator<Etablissement>() {
                @Override
                public int compare(Etablissement o1, Etablissement o2) {
                    int NameCompare = o2.getNom().compareTo(o1.getNom());
                    sorted=0;
                    // 2-level comparison using if-else block
                    return NameCompare;

                }

            });
        }
        notifyDataSetChanged();
    }

    public void searchList(String nom,List<Etablissement> et){

        if(nom==null || nom.equalsIgnoreCase("")){

        }else{
            List<Etablissement> el = new ArrayList<>();
            listET = new ArrayList<>(et);

            for(Etablissement e:et){
                if(!e.getNom().equalsIgnoreCase(nom)) {
                    System.out.println(e.getNom() + "\t" + nom);
                    el.add(e);
                }
            }

            for(Etablissement e:el){
                listET.remove(listET.remove(e));
            }

            notifyDataSetChanged();
        }


    }

    @Override
    public void notifyDataSetChanged() {
        //do your sorting here
        /**/

        super.notifyDataSetChanged();
    }


    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    public List<Etablissement> getListET() {
        return listET;
    }

    public void setListET(List<Etablissement> listET) {
        this.listET = listET;
    }

    public int getSorted() {
        return sorted;
    }

    public void setSorted(int sorted) {
        this.sorted = sorted;
    }
}
