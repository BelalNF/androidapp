package ensat.androidapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import ensat.androidapp.Dao.MyDBHandler;
import ensat.androidapp.Models.Etablissement;

public class MyEditActivity extends AppCompatActivity {

    EditText nomEditText;
    EditText adresseEditText;

    ImageView imageview;
    Bitmap logo;
    Etablissement et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));

        //----------------------------
        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            String jsonObject = extras.getString("et");
            et = new Gson().fromJson(jsonObject, Etablissement.class);

            //toolbar.setSubtitle(et.getNom());
            nomEditText = findViewById(R.id.editText);
            nomEditText.setText(et.getNom());

            adresseEditText = findViewById(R.id.editText2);
            adresseEditText.setText(et.getAdresse());

            imageview = findViewById(R.id.imageView3);
            imageview.setImageBitmap(et.getLogo());

            //set formation on spinner
            Spinner formationSpinner = findViewById(R.id.spinner3);
            String compareValue = et.getFormation();
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.formations, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            formationSpinner.setAdapter(adapter);
            if (compareValue != null) {
                int spinnerPosition1 = adapter.getPosition(compareValue);
                formationSpinner.setSelection(spinnerPosition1);
            }

            //set specialite on spinner
            Spinner specSpinner = findViewById(R.id.spinner4);
            String compareValue2 = et.getSpecialite();
            ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.specialite, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            specSpinner.setAdapter(adapter2);
            if (compareValue != null) {
                int spinnerPosition2 = adapter.getPosition(compareValue2);
                specSpinner.setSelection(spinnerPosition2);
            }

            Button editLogoButton = findViewById(R.id.editLogoButton);
            editLogoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    final int ACTIVITY_SELECT_IMAGE = 1234;
                    startActivityForResult(i, ACTIVITY_SELECT_IMAGE);

                    Toast.makeText(getApplicationContext(), "TEdit Logo Button fired",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Button saveButton = findViewById(R.id.saveButton2);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "TEdit Button fired",
                            Toast.LENGTH_SHORT).show();
                    MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

                    String nom = nomEditText.getText().toString();
                    String adresse = adresseEditText.getText().toString();
                    String formation =  ( (Spinner) findViewById(R.id.spinner3) ).getSelectedItem().toString();
                    String specialite = ( (Spinner) findViewById(R.id.spinner4) ).getSelectedItem().toString();

                    et.setNom(nom);
                    if(logo != null) et.setLogo(logo);
                    et.setAdresse(adresse);
                    et.setFormation(formation);
                    et.setSpecialite(specialite);

                    dbHandler.updateHandler(et);

                    startActivity(new Intent(getApplicationContext(),MainActivity.class));

                }
            });

            Button deleteButton = findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


                    boolean isDeleted = dbHandler.deleteHandler(et.getId());

                    if(isDeleted){
                        Toast.makeText(getApplicationContext(), "Row deleted ",
                                Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(getApplicationContext(), " Not deleted",
                                Toast.LENGTH_SHORT).show();

                    }

                    dbHandler.deleteHandler(et.getId());

                    startActivity(new Intent(getApplicationContext(),MainActivity.class));


                }
            });
            // and get whatever type user account id is

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 1234:
                if(resultCode == RESULT_OK){
                    try {

                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String filePath = cursor.getString(columnIndex);
                        cursor.close();


                        logo = BitmapFactory.decodeFile(filePath);

                        imageview.setImageBitmap(logo);


                    }catch(Exception ex){
                        Toast.makeText(this, "Error"+ex.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                    /* Now you have choosen image in Bitmap format in object "yourSelectedImage". You can use it in way you want! */
                }
        }

    };

}
