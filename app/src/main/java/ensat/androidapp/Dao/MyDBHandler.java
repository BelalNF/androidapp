package ensat.androidapp.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import ensat.androidapp.Models.Etablissement;
import ensat.androidapp.R;

public class MyDBHandler extends SQLiteOpenHelper {
    //information of database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "etablissementDB.db";
    public static final String TABLE_NAME = "Etablissement";
    public static final String COLUMN_ID = "Id";
    Context con;

    //initialize the database
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        con = context;
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                " Nom TEXT NOT NULL,"+
                " Logo BLOB NOT NULL,"+
                " Adresse TEXT NOT NULL,"+
                " Formation TEXT NOT NULL,"+
                " Specialite TEXT NOT NULL );"
                ;
        db.execSQL(CREATE_TABLE);
        System.out.println("onCreate called");
        //=====================

    }



    public void generateAllDefaultData(List<Etablissement> etList){

        String query = "Select * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int items = cursor.getCount();
        db.close();

        //First Time we open Database, add default Values
        if ( items < 1 )
        {

            for(Etablissement e:etList) this.addHandler(e);
            //first et
            /*Etablissement et = new Etablissement("CESIM, Ecole Supérieure d'Ingénieurs à Tanger",BitmapFactory.decodeResource(con.getResources(),
                    R.drawable.suptemlogo_background),"Villa CESIM -, 197 Avenue Haroun Errachid, Tanger 90000","Initiale","Managment");

            this.addHandler(et);
            //first et
            et = new Etablissement("École nationale de commerce et de gestion de Tanger",BitmapFactory.decodeResource(con.getResources(),
                    R.drawable.encglogo_background),"Messnana TANGER","Initiale","Managment");
            this.addHandler(et);
            //first et
            et = new Etablissement("Ecole Nationale des Sciences Appliquées de Tanger",BitmapFactory.decodeResource(con.getResources(),
                    R.drawable.ensatlogo_background),"Boukhalef Tanger","Initiale","Ingenierie");
            this.addHandler(et);*/
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

/*    public Etablissement findHandler(String studentname) {
        return
    }*/

    public List<Etablissement> loadHandler() {

        String query = "Select * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        List<Etablissement> EtList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Integer Id = cursor.getInt(0);
            String Nom = cursor.getString(1);
            //convert blob image stored in sqLite to bitmap object
            byte[] byteArray = cursor.getBlob(2);
            Bitmap Logo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            String Adresse = cursor.getString(3);
            String Formation = cursor.getString(4);
            String Specialite = cursor.getString(5);

            Etablissement et = new Etablissement(Id,Nom,Logo,Adresse,Formation,Specialite);

            EtList.add(et);
        }

        cursor.close();
        db.close();
        return EtList;
    }

    public void addHandler(Etablissement etb) {
        ContentValues values = new ContentValues();
        values.put("Nom", etb.getNom());
        //Bitmap image to byte
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        etb.getLogo().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        values.put("Logo", byteArray);
        values.put("Adresse", etb.getAdresse());
        values.put("Formation", etb.getFormation());
        values.put("Specialite", etb.getSpecialite());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public boolean updateHandler(Etablissement etb) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Nom", etb.getNom());
        //Bitmap image to byte
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        etb.getLogo().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        values.put("Logo", byteArray);
        values.put("Adresse", etb.getAdresse());
        values.put("Formation", etb.getFormation());
        values.put("Specialite", etb.getSpecialite());
        return db.update(TABLE_NAME, values, COLUMN_ID + "=" + etb.getId(), null) > 0;
    }

    public boolean deleteHandler(int ID) {

        return this.getWritableDatabase().delete(TABLE_NAME, COLUMN_ID + "=" + ID, null) > 0;

    }
}
