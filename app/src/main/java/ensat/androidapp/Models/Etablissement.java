package ensat.androidapp.Models;

import android.graphics.Bitmap;

public class Etablissement {

    private Integer Id;
    private String Nom;
    private Bitmap Logo;
    private String Adresse;
    private String formation;
    private String specialite;

    public Etablissement() {
    }

    public Etablissement(Integer id, String nom, Bitmap logo, String adresse, String formation, String specialite) {
        Id = id;
        Nom = nom;
        Logo = logo;
        Adresse = adresse;
        this.formation = formation;
        this.specialite = specialite;
    }

    public Etablissement(String nom, Bitmap logo, String adresse, String formation, String specialite) {
        Nom = nom;
        Logo = logo;
        Adresse = adresse;
        this.formation = formation;
        this.specialite = specialite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public Bitmap getLogo() {
        return Logo;
    }

    public void setLogo(Bitmap logo) {
        Logo = logo;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    @Override
    public String toString() {
        return "Etablissement{" +
                "Nom='" + Nom + '\'' +
                ", Logo='" + Logo + '\'' +
                ", Adresse='" + Adresse + '\'' +
                ", formation='" + formation + '\'' +
                ", specialite='" + specialite + '\'' +
                '}';
    }
}
