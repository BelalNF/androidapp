package ensat.androidapp.ui.Acceuil;

import ensat.androidapp.CustomAdapter;
import ensat.androidapp.MyEditActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ensat.androidapp.Dao.MyDBHandler;
import ensat.androidapp.Models.Etablissement;
import ensat.androidapp.R;

public class AcceuilFragment extends Fragment implements AdapterView.OnItemClickListener {

    private AcceuilViewModel acceuilViewModel;
    private String[] m;
    List<Etablissement> listET;
    EditText searchEditText;
    CustomAdapter customAdapter;
    int sorted;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        acceuilViewModel =
                ViewModelProviders.of(this).get(AcceuilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        acceuilViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        searchEditText = root.findViewById(R.id.searcheditText2);

        ListView listView = root.findViewById(R.id.listview);

        MyDBHandler dbHandler = new MyDBHandler(getActivity(), null, null, 1);

        //================================================================================================================

        //List<Etablissement> etList = new ArrayList<>();

        //Bitmap bmp = BitmapFactory.decodeResource(this.getResources(), R.mipmap.encglogo);

        //etList.add(new Etablissement("CESIM, Ecole Supérieure d'Ingénieurs à Tanger", bmp ,"Villa CESIM -, 197 Avenue Haroun Errachid, Tanger 90000","Initiale","Managment"));
        //etList.get(0).setLogo(bmp);
        //System.out.println(etList.get(0));
        /*//first et
        etList.add(new Etablissement("École nationale de commerce et de gestion de Tanger",BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.encglogo_background),"Messnana TANGER","Initiale","Managment"));
        //first et
        etList.add(new Etablissement("Ecole Nationale des Sciences Appliquées de Tanger",BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.ensatlogo_background),"Boukhalef Tanger","Initiale","Ingenierie"));
*/
        //=============================================================================================================
        //dbHandler.generateAllDefaultData(etList);


        listET = dbHandler.loadHandler();

        customAdapter = new CustomAdapter(getActivity().getLayoutInflater(),listET);

        listView.setAdapter(customAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getActivity(), " jsonobject "+ new Gson().toJson(listET.get(position)),
                            Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(getActivity(), MyEditActivity.class );
                    //pass selected object to new activity
                    in.putExtra("et", new Gson().toJson(listET.get(position)));
                    getActivity().startActivity(in);

            }
            });

        //add searchbtin listner
        Button searchbtn = root.findViewById(R.id.searchbtn);

        searchbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                customAdapter.searchList(searchEditText.getText().toString(),listET);
            }
        });

        //add sortBtn listner
        Button sortbtn = root.findViewById(R.id.sortbtn);

        sortbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                customAdapter.sortList();
            }
        });

        return root;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
