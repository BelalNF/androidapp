package ensat.androidapp.ui.ajouter;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import ensat.androidapp.Dao.MyDBHandler;
import ensat.androidapp.MainActivity;
import ensat.androidapp.Models.Etablissement;
import ensat.androidapp.R;

import static android.app.Activity.RESULT_OK;

public class AjouterFragment extends Fragment {

    private AjouterViewModel ajouterViewModel;
    Button addButton;
    Button insertImgButton;
    Bitmap logo;
    ImageView imageView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ajouterViewModel =
                ViewModelProviders.of(this).get(AjouterViewModel.class);
        View root = inflater.inflate(R.layout.fragment_add, container, false);
        ajouterViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        imageView = root.findViewById(R.id.imageView4);

        addButton = root.findViewById(R.id.buttonadd);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    MyDBHandler dbHandler = new MyDBHandler(getActivity(), null, null, 1);

                String nom = ( (EditText)getActivity().findViewById(R.id.nomEditText) ).getText().toString();
                String adresse = ( (EditText)getActivity().findViewById(R.id.adresseEditText) ).getText().toString();
                String formation =  ( (Spinner) getActivity().findViewById(R.id.spinner2) ).getSelectedItem().toString();
                String specialite = ( (Spinner) getActivity().findViewById(R.id.spinner) ).getSelectedItem().toString();

                Etablissement etb = new Etablissement(nom,logo,adresse,formation,specialite);

                dbHandler.addHandler(etb);

                Toast.makeText(getActivity(), "Etablissement ajouter avec succès "+etb,
                        Toast.LENGTH_LONG).show();

                startActivity(new Intent(getContext(), MainActivity.class));


            }
        });

        //add action listner to select image button

        insertImgButton = root.findViewById(R.id.insertImgButton);
        insertImgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                final int ACTIVITY_SELECT_IMAGE = 1234;
                startActivityForResult(i, ACTIVITY_SELECT_IMAGE);

                Toast.makeText(getActivity(), "This is my Toast message! ",
                        Toast.LENGTH_LONG).show();
            }
        });


        return root;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 1234:
                if(resultCode == RESULT_OK){
                    try {

                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);//return 0
                        String filePath = cursor.getString(columnIndex);
                        cursor.close();


                        logo = BitmapFactory.decodeFile(filePath);//null

                        imageView.setImageBitmap(logo);

                    }catch(Exception ex){
                        Toast.makeText(getActivity(), "Error"+ex.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
        }

    };

}